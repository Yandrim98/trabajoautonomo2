package edu.uoc.android.restservice.ui.enter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import edu.uoc.android.restservice.R;
import edu.uoc.android.restservice.rest.adapter.GitHubAdapter;
import edu.uoc.android.restservice.rest.model.Owner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InfoUserActivity extends AppCompatActivity {

    ArrayList<Owner> listaFollowers;
    RecyclerView recyclerViewFollowers;
    TextView textViewRepositories, textViewFollowing, textViewLogin;
    ImageView imageViewProfile;
    ProgressBar progressBar;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_user);

        textViewLogin = (TextView) findViewById(R.id.textViewLogin);
        linearLayout = (LinearLayout) findViewById(R.id.Datos);
        textViewFollowing = findViewById(R.id.textViewFollowing);
        textViewRepositories = findViewById(R.id.textViewRepositories);
        imageViewProfile = (ImageView) findViewById(R.id.imageViewProfile);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        listaFollowers = new ArrayList<>();
        recyclerViewFollowers = (RecyclerView) findViewById(R.id.recyclerViewFollowers);
        recyclerViewFollowers.setLayoutManager(new LinearLayoutManager(this));
        String loginName = getIntent().getStringExtra("loginName");
        initProgressBar();
        //Se muestran los datos basicos
        VerDatosB(loginName);
        //Se muestran los datos completos
        VerDatosComp(loginName);
    }

    private void initProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    //metodo para ver los datos basicos del usuario
    private void VerDatosB(final String loginName) {
        GitHubAdapter adapter = new GitHubAdapter();
        Call<Owner> call = adapter.getOwner(loginName);
        call.enqueue(new Callback<Owner>() {

            @Override
            public void onResponse(Call<Owner> call, Response<Owner> response) {
                Owner owner = response.body();
                textViewRepositories.setText(owner.getPublicRepos().toString());
                textViewFollowing.setText(owner.getFollowing().toString());
                textViewLogin.setText(owner.getLogin());
                Picasso.get().load(owner.getAvatarUrl()).into(imageViewProfile);
            }

            @Override
            public void onFailure(Call<Owner> call, Throwable t) {

            }
        });
    }


//Metodo para mostrar datos comple. seguidores, etc//
    private void VerDatosComp(String loginName){
        GitHubAdapter gitHubAdapter = new GitHubAdapter();
        Call<List<Owner>> call = gitHubAdapter.getOwnerFollowers(loginName);
        call.enqueue(new Callback<List<Owner>>() {
            @Override
            public void onResponse(Call<List<Owner>> call, Response<List<Owner>> response) {
                List<Owner> ownerList = response.body();
                for (Owner owner: ownerList){
                    listaFollowers.add(owner);
                }
                AdaptadorFollowers adaptadorFollowers = new AdaptadorFollowers(listaFollowers);
                recyclerViewFollowers.setAdapter(adaptadorFollowers);
                progressBar.setVisibility(View.GONE);
                linearLayout.setVisibility(View.VISIBLE);
            }
            @Override
            public void onFailure(Call<List<Owner>> call, Throwable t) {

            }
        });
    }

}
